import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import AllWallet from 'src/screens/Main/BottomTab/AllWallet'
import Profile from 'src/screens/Main/BottomTab/Profile'
import colors from 'src/themes/colors'
import Icon from 'react-native-vector-icons/Ionicons'

const Tab = createBottomTabNavigator()

export default () => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: colors.primary,
            }}
            initialRouteName="AllWallet"
        >
            <Tab.Screen
                name="AllWallet"
                component={AllWallet}
                options={{
                    title: 'My Piggy',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="ios-wallet" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    title: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="ios-contact" color={color} size={size} />
                    ),
                }}
            />
        </Tab.Navigator>
    )
}
