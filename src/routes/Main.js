import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import BottomTab from './BottomTab'
import ChangeProfileImage from 'src/screens/Main/ChangeProfileImage'
import Wallet from 'src/screens/Main/Wallet'
import AddWallet from 'src/screens/Main/AddWallet'
import AddTransaction from 'src/screens/Main/AddTransaction'
import Transaction from 'src/screens/Main/Transaction'
import EditProfile from 'src/screens/Main/EditProfile'
import ChangePassword from 'src/screens/Main/ChangePassword'

const Stack = createStackNavigator()

export default () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="BottomTab">
            <Stack.Screen name="BottomTab" component={BottomTab} />
            <Stack.Screen name="ChangeProfileImage" component={ChangeProfileImage} />
            <Stack.Screen name="Wallet" component={Wallet} />
            <Stack.Screen name="AddWallet" component={AddWallet} />
            <Stack.Screen name="AddTransaction" component={AddTransaction} />
            <Stack.Screen name="Transaction" component={Transaction} />
            <Stack.Screen name="EditProfile" component={EditProfile} />
            <Stack.Screen name="ChangePassword" component={ChangePassword} />
        </Stack.Navigator>
    )
}
