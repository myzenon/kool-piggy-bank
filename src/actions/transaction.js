export const ADD_TRANSACTION = 'ADD_TRANSACTION'

export const addTransaction = (name, mode, amount, walletId, userId) => ({
    type: ADD_TRANSACTION,
    name,
    mode,
    amount: parseInt(amount),
    walletId,
    userId,
})
