export const ADD_WALLET = 'ADD_WALLET'

export const addWallet = (name, initialAmount, userId) => ({
    type: ADD_WALLET,
    name,
    initialAmount: parseInt(initialAmount),
    userId,
})
