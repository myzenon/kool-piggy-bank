import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    containerContent: {
        flexGrow: 1,
        paddingBottom: 24,
    },
    headerContainer: {
        width: '100%',
        backgroundColor: '#FFF',
    },
    headerInsetContainer: {
        paddingVertical: 96,
        paddingHorizontal: 24,
    },
    totalContainer: {
        backgroundColor: '#f27580',
        padding: 16,
        elevation: 4,
        borderRadius: 999,
        transform: [ { rotate: '2deg' } ],
    },
    totalText: {
        fontSize: 36,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#FFF',
    },
    toolbarContainer: {
        flexDirection: 'row',
        marginHorizontal: 32,
        marginBottom: 18,
        alignItems: 'center',
    },
    toolbarLeftContainer: {
        flex: 1,
        marginRight: 12,
    },
    profileNameText: {
        fontWeight: 'bold',
    },
    addWalletButton: {
        backgroundColor: '#FFE0E3',
    },
    walletContainer: {
        marginHorizontal: 32,
        marginVertical: 8,
        borderWidth: 1,
        borderColor: '#EEE',
        borderRadius: 12,
        flexDirection: 'row',
    },
    walletIconContainer: {
        paddingVertical: 24,
        paddingHorizontal: 18,
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12,
        backgroundColor: '#FCECEE',
    },
    walletTextContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 12,
    },
    walletNameText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#333',
    },
    walletDateText: {
        color: '#AAA',
        fontSize: 14,
    },
    walletAmountContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingHorizontal: 12,
    },
    emptyListContainer: {
        marginTop: 24,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    emptyListMainText: {
        fontWeight: 'bold',
    },
    emptyListSubText: {
        fontSize: 36,
        color: '#555',
    },
    emptyListAddWalletButton: {
        marginTop: 32,
        justifyContent: 'center',
        alignItems: 'center',
    },
    emptyListAddWalletButtonText: {
        marginTop: 12,
    },
})
