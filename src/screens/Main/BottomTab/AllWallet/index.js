import React, { useMemo } from 'react'
import { FlatList, View, Text, ImageBackground, TouchableOpacity } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import styles from './styles'
import { Chip } from 'react-native-paper'
import { useSelector } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'
import { useNavigation } from '@react-navigation/native'
import { Avatar } from 'react-native-paper'
import { fundComma, dateTime } from 'src/helpers/text'

export default function AllWallet() {
    const profile = useSelector(state => state.profile)
    const wallets = useSelector(state => state.wallet).filter((wallet) => wallet.userId === profile._id)
    const navigation = useNavigation()

    const totalAmount = useMemo(() => {
        return fundComma(wallets.reduce((sum, wallet) => {
            sum += wallet.amount
            return sum
        }, 0))
    }, [ wallets ])

    return (
        <FlatList
            data={wallets}
            keyExtractor={(item, index) => index + ''}
            style={styles.container}
            contentContainerStyle={styles.containerContent}
            ListHeaderComponent={
                <>
                    <ImageBackground
                        source={require('src/assets/allwallet.png')}
                        style={styles.headerContainer}
                        resizeMode="cover"
                    >
                        <SafeAreaView>
                            <View style={styles.headerInsetContainer}>
                                <View style={styles.totalContainer}>
                                    <Text style={styles.totalText}>{totalAmount} ฿</Text>
                                </View>
                            </View>
                        </SafeAreaView>
                    </ImageBackground>
                    {
                        wallets.length > 0 ?
                            (
                                <View style={styles.toolbarContainer}>
                                    <View style={styles.toolbarLeftContainer}>
                                        <Text numberOfLines={1}>
                                            WELCOME, <Text style={styles.profileNameText}>{profile.name}</Text>
                                        </Text>
                                    </View>
                                    <Chip
                                        icon="plus"
                                        style={styles.addWalletButton}
                                        onPress={() => navigation.navigate('AddWallet')}
                                    >
                                        ADD WALLET
                                    </Chip>
                                </View>
                            )
                            : null
                    }
                </>
            }
            renderItem={({ item: wallet }) => (
                <TouchableOpacity style={styles.walletContainer} onPress={() => navigation.navigate('Wallet', { wallet })}>
                    <View style={styles.walletIconContainer}>
                        <Icon name="ios-wallet" size={24} color="#F7A5AC" />
                    </View>
                    <View style={styles.walletTextContainer}>
                        <Text style={styles.walletNameText}>{wallet.name}</Text>
                        <Text style={styles.walletDateText}>{dateTime(wallet.createdDate)}</Text>
                    </View>
                    <View style={styles.walletAmountContainer}>
                        <Text>{fundComma(wallet.amount)} ฿</Text>
                    </View>
                </TouchableOpacity>
            )}
            ListEmptyComponent={
                <View style={styles.emptyListContainer}>
                    <Text style={styles.emptyListMainText}>WELCOME</Text>
                    <Text style={styles.emptyListSubText}>{profile.name}</Text>
                    <TouchableOpacity
                        style={styles.emptyListAddWalletButton}
                        onPress={() => navigation.navigate('AddWallet')}
                    >
                        <Avatar.Icon size={192} icon="plus" />
                        <Text style={styles.emptyListAddWalletButtonText}>To get start, Add New Wallet</Text>
                    </TouchableOpacity>
                </View>
            }
        />
    )
}
