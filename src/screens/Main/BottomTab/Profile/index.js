import React, { useState, useCallback, useEffect } from 'react'
import { ScrollView, View, Text, ImageBackground, TouchableOpacity } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import styles from './styles'
import { Avatar, Chip } from 'react-native-paper'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { List } from 'react-native-paper'
import { dateMonth } from 'src/helpers/text'
import imagePicker from 'src/helpers/imagePicker'
import { setProfile } from 'src/actions/profile'
import profileAPI from 'src/api/profile'
import authAPI from 'src/api/auth'
import { setAuthToken } from 'src/actions/authToken'

export default function Profile() {
    const token = useSelector(state => state.authToken)
    const profile = useSelector(state => state.profile)
    const [ profileImage, setProfileImage ] = useState({ uri: profile.profileImage })
    const navigation = useNavigation()
    const dispatch = useDispatch()

    const logout = useCallback(() => {
        authAPI.logout(token)
            .finally(() => {
                dispatch(setAuthToken(null))
            })
    }, [ dispatch, token ])

    const changeProfileImage = useCallback(async () => {
        try {
            const image = await imagePicker()
            setProfileImage(image)
            profileAPI.updateImage(image, token)
                .then(() => {
                    getProfile()
                })
                .catch(error => {})
        }
        catch (error) {}
    }, [ getProfile, token ])

    const getProfile = useCallback(() => {
        profileAPI.get(token)
            .then((profile) => {
                dispatch(setProfile(profile))
            })
            .catch(error => {})
    }, [ dispatch, token ])

    useEffect(() => {
        getProfile()
    }, [ getProfile ])

    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainerStyle}>
            <ImageBackground
                source={require('src/assets/profile.png')}
                style={styles.headerContainer}
            >
                <SafeAreaView style={styles.headerInsetContainer}>
                    <TouchableOpacity onPress={changeProfileImage}>
                        <Avatar.Image size={192} source={profileImage} />
                    </TouchableOpacity>
                </SafeAreaView>
            </ImageBackground>
            <View style={styles.profileContainer}>
                <Text style={styles.profileUsernameText}>{profile.username}</Text>
                <Text style={styles.profileNameText}>{profile.name}</Text>
                <View style={styles.profileInfoContainer}>
                    <Chip icon="cake" style={styles.profileInfoChip}>
                        {dateMonth(profile.dateOfBirth)}
                    </Chip>
                    <Chip icon="map-marker" style={styles.profileInfoChip}>
                        {profile.address}
                    </Chip>
                </View>
            </View>
            <View style={styles.listContainer}>
                <List.Item
                    title="Edit Profile"
                    description="Edit the your profile information"
                    left={props => <List.Icon {...props} icon="account-edit" />}
                    onPress={() => navigation.navigate('EditProfile')}
                />
                <List.Item
                    title="Change Password"
                    description="Change your current password"
                    left={props => <List.Icon {...props} icon="key" />}
                    onPress={() => navigation.navigate('ChangePassword')}
                />
                <List.Item
                    title="Transaction"
                    description="View all your transactions in every wallet"
                    left={props => <List.Icon {...props} icon="cash-usd" />}
                    onPress={() => navigation.navigate('Transaction')}
                />
                <List.Item
                    title="Logout"
                    description="Sign out from the system"
                    left={props => <List.Icon {...props} icon="logout-variant" />}
                    onPress={logout}
                />
            </View>
        </ScrollView>
    )
}
