import React, { useState, useCallback } from 'react'
import { ScrollView, View, Text, TouchableOpacity } from 'react-native'
import styles from './styles'
import { Button } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient'
import profileAPI from 'src/api/profile'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { SafeAreaView } from 'react-native-safe-area-context'
import colors from 'src/themes/colors'
import { Avatar } from 'react-native-paper'
import imagePicker from 'src/helpers/imagePicker'
import { setProfile } from 'src/actions/profile'

export default function ChangeProfileImage () {
    const [ profileImage, setProfileImage ] = useState(null)
    const [ isLoadingSubmit, setIsLoadingSubmit ] = useState(false)
    const token = useSelector(state => state.authToken)
    const navigation = useNavigation()
    const dispatch = useDispatch()

    const changeProfileImage = useCallback(async () => {
        try {
            const image = await imagePicker()
            setProfileImage(image)
        }
        catch (error) {}
    }, [])

    const submit = useCallback(() => {
        setIsLoadingSubmit(true)
        profileAPI.updateImage(profileImage, token)
            .then(() => {
                profileAPI.get(token)
                    .then((profile) => {
                        dispatch(setProfile(profile))
                        navigation.navigate('AllWallet')
                    })
            })
            .finally(() => {
                setIsLoadingSubmit(false)
            })
    }, [ navigation, profileImage, token, dispatch ])

    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainerStyle}>
            <SafeAreaView style={styles.imageContainer}>
                <View style={styles.headerTextContainer}>
                    <Text style={styles.mainText}>Wanna see your smile :)</Text>
                    <Text style={styles.subText}>Please select your image profile</Text>
                </View>
                <TouchableOpacity onPress={changeProfileImage}>
                    {
                        !profileImage ?
                            (
                                <Avatar.Icon
                                    size={256}
                                    icon="camera"
                                    style={styles.avatar}
                                />
                            )
                            :
                            (
                                <Avatar.Image
                                    size={256}
                                    source={{ uri: profileImage.uri }}
                                    style={styles.avatar}
                                />
                            )
                    }
                </TouchableOpacity>
                {
                    profileImage ?
                        (
                            <Button
                                title="SUBMIT"
                                ViewComponent={LinearGradient}
                                linearGradientProps={{
                                    colors: [ colors.secondary, colors.primary ],
                                    start: { x: 0, y: 0 },
                                    end: { x: 1, y: 1 },
                                }}
                                raised
                                buttonStyle={styles.submitButton}
                                onPress={submit}
                                loading={isLoadingSubmit}
                            />
                        ) : null
                }
            </SafeAreaView>
        </ScrollView>
    )
}
