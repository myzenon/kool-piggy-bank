import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    contentContainerStyle: {
        flexGrow: 1,
        paddingBottom: 16,
    },
    imageContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        borderWidth: 1,
        borderColor: '#555',
        borderStyle: 'dashed',
        marginBottom: 64,
    },
    headerTextContainer: {
        marginBottom: 64,
    },
    mainText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 24,
    },
    subText: {
        textAlign: 'center',
        fontSize: 20,
        color: '#888',
    },
    submitButton: {
        paddingHorizontal: 96,
        borderRadius: 999,
    },
})
