import React, { useState, useCallback, useRef, useEffect } from 'react'
import { View, ScrollView, TouchableOpacity, Alert, Image } from 'react-native'
import { Button } from 'react-native-elements'
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import LinearGradient from 'react-native-linear-gradient'
import { useNavigation } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux'
import styles from './styles'
import { SafeAreaView } from 'react-native-safe-area-context'
import { TextInput } from 'react-native-paper'
import colors from 'src/themes/colors'
import profileAPI from 'src/api/profile'
import { setProfile } from 'src/actions/profile'
import Header from 'src/components/Header'
import { dateMonth } from 'src/helpers/text'

const showWarningPopup = (message) => Alert.alert(
    'กรุณาตรวจสอบข้อมูล',
    message,
)

export default function EditProfile () {
    const token = useSelector(state => state.authToken)
    const profile = useSelector(state => state.profile)
    const navigation = useNavigation()
    const [ name, setName ] = useState('')
    const [ address, setAddress ] = useState('')
    const [ dob, setDOB ] = useState(new Date('1990-01-01'))
    const [ isLoadingSubmit, setIsLoadingSubmit ] = useState(false)
    const [ isDatePickerVisible, setIsDatePickerVisble ] = useState(false)
    const dispatch = useDispatch()

    const addressInput = useRef()

    const changeDOB = useCallback((date) => {
        setIsDatePickerVisble(false)
        setDOB(date)
        addressInput.current.focus()
    }, [ setDOB, setIsDatePickerVisble ])

    const validateForm = useCallback(() => {
        if (!name) {
            showWarningPopup('กรุณากรอกชื่อ')
        }
        else if (!dob) {
            showWarningPopup('กรุณากรอกวันเกิด')
        }
        else if (!address) {
            showWarningPopup('กรุณากรอกที่อยู่')
        }
        else {
            return true
        }
        return false
    }, [ address, dob, name ])

    const submit = useCallback(() => {
        if (validateForm()) {
            setIsLoadingSubmit(true)
            profileAPI.update({
                name,
                address,
                dateOfBirth: dob.getTime(),
            }, token)
                .then((profile) => {
                    dispatch(setProfile(profile))
                    navigation.goBack()
                })
                .finally(() => {
                    setIsLoadingSubmit(false)
                })
        }
    }, [ address, dispatch, dob, name, navigation, token, validateForm ])

    useEffect(() => {
        setName(profile.name)
        setDOB(new Date(profile.dateOfBirth))
        setAddress(profile.address)
    }, [ profile ])

    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainerStyle}>
            <SafeAreaView style={styles.contentContaier}>
                <Header title="Edit Profile" />
                <Image
                    source={require('src/assets/editprofile.png')}
                    resizeMode="cover"
                    style={styles.editProfileImage}
                />
                <View style={styles.formContainer}>
                    <TextInput
                        label="Name"
                        mode="outlined"
                        style={styles.input}
                        value={name}
                        onChangeText={setName}
                        textContentType="name"
                        returnKeyType="next"
                        onSubmitEditing={() => setIsDatePickerVisble(true)}
                    />
                    <TouchableOpacity onPress={() => setIsDatePickerVisble(true)}>
                        <TextInput
                            label="Date of Birth"
                            mode="outlined"
                            style={styles.input}
                            value={dateMonth(dob)}
                            editable={false}
                        />
                    </TouchableOpacity>
                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={changeDOB}
                        onCancel={() => setIsDatePickerVisble(false)}
                    />
                    <TextInput
                        ref={addressInput}
                        label="Address"
                        mode="outlined"
                        style={styles.input}
                        value={address}
                        onChangeText={setAddress}
                        textContentType="fullStreetAddress"
                        multiline
                    />
                    <View style={styles.actionContainer}>
                        <Button
                            title="CHANGE PROFILE"
                            ViewComponent={LinearGradient}
                            linearGradientProps={{
                                colors: [ colors.secondary, colors.primary ],
                                start: { x: 0, y: 0 },
                                end: { x: 1, y: 1 },
                            }}
                            raised
                            buttonStyle={styles.changeProfileButton}
                            onPress={submit}
                            loading={isLoadingSubmit}
                        />
                    </View>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}
