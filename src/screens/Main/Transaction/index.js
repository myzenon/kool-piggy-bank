import React from 'react'
import { FlatList, View, Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import styles from './styles'
import Icon from 'react-native-vector-icons/Ionicons'
import { Avatar } from 'react-native-paper'
import Header from 'src/components/Header'
import { useSelector } from 'react-redux'
import { fundComma, dateTime } from 'src/helpers/text'


export default function Transaction(props) {
    const profile = useSelector(state => state.profile)
    const wallets = useSelector(state => state.wallet).reduce((obj, wallet) => {
        obj[wallet.id] = wallet
        return obj
    }, {})
    const transactions = useSelector(state => state.transaction).filter(transaction => transaction.userId === profile._id).reverse()

    return (
        <FlatList
            data={transactions}
            keyExtractor={(item, index) => index + ''}
            style={styles.container}
            contentContainerStyle={styles.containerContent}
            ListHeaderComponent={
                <>
                    <SafeAreaView>
                        <Header title="All Transaction" />
                    </SafeAreaView>
                </>
            }
            renderItem={({ item: transaction }) => (
                <View style={styles.transactionContainer}>
                    <View style={styles.transactionIconContainer}>
                        {
                            transaction.mode === 'INCOME' ?
                                <Icon name="ios-add" size={24} color="#F7A5AC" />
                                :
                                <Icon name="ios-remove" size={24} color="#F7A5AC" />
                        }
                    </View>
                    <View style={styles.transactionTextContainer}>
                        <Text style={styles.transactionNameText}>{wallets[transaction.walletId].name} / {transaction.name}</Text>
                        <Text style={styles.transactionDateText}>{dateTime(transaction.createdDate)}</Text>
                    </View>
                    <View style={styles.transactionAmountContainer}>
                        {
                            transaction.mode === 'INCOME' ?
                                <Text style={styles.transactionAmountIncomeText}>+ {fundComma(transaction.amount)} ฿</Text>
                                :
                                <Text style={styles.transactionAmountExpenseText}>- {fundComma(transaction.amount)} ฿</Text>
                        }
                    </View>
                </View>
            )}
            ListEmptyComponent={
                <View style={styles.emptyListContainer}>
                    <Text style={styles.emptyListMainText}>Your transaction is</Text>
                    <Text style={styles.emptyListSubText}>Empty</Text>
                    <View style={styles.emptyListAddTransactionButton}>
                        <Avatar.Icon size={192} icon="cash" />
                        <Text style={styles.emptyListAddTransactionButtonText}>Please add at least one transaction before use this page.</Text>
                    </View>
                </View>
            }
        />
    )
}
