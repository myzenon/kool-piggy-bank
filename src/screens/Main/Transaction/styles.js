import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    containerContent: {
        flexGrow: 1,
        paddingBottom: 24,
    },
    headerContainer: {
        width: '100%',
        backgroundColor: '#FFF',
    },
    headerInsetContainer: {
        paddingTop: 128,
        paddingHorizontal: 24,
    },
    totalContainer: {
        backgroundColor: '#f27580',
        padding: 16,
        elevation: 4,
        borderRadius: 999,
        transform: [ { rotate: '358deg' } ],
    },
    totalText: {
        fontSize: 36,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#FFF',
    },
    toolbarContainer: {
        flexDirection: 'row',
        marginTop: 24,
        marginHorizontal: 32,
        marginBottom: 18,
        alignItems: 'center',
    },
    toolbarLeftContainer: {
        flex: 1,
        marginRight: 12,
    },
    walletNameText: {
        fontWeight: 'bold',
    },
    addTransactionButton: {
        backgroundColor: '#FFE0E3',
    },
    transactionContainer: {
        marginHorizontal: 32,
        marginVertical: 8,
        borderWidth: 1,
        borderColor: '#EEE',
        borderRadius: 12,
        flexDirection: 'row',
    },
    transactionIconContainer: {
        paddingVertical: 24,
        paddingHorizontal: 18,
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12,
        backgroundColor: '#FCECEE',
    },
    transactionTextContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 12,
    },
    transactionNameText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#333',
    },
    transactionDateText: {
        color: '#AAA',
        fontSize: 14,
    },
    transactionAmountContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingHorizontal: 12,
    },
    transactionAmountIncomeText: {
        color: 'green',
    },
    transactionAmountExpenseText: {
        color: 'red',
    },
    emptyListContainer: {
        marginTop: 42,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    emptyListMainText: {
        fontWeight: 'bold',
    },
    emptyListSubText: {
        fontSize: 36,
        color: '#555',
    },
    emptyListAddTransactionButton: {
        marginTop: 32,
        justifyContent: 'center',
        alignItems: 'center',
    },
    emptyListAddTransactionButtonText: {
        marginTop: 12,
    },
})
