import React, { useState, useCallback, useRef, useMemo } from 'react'
import { ScrollView, View, Image, Alert, TouchableOpacity, Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import styles from './styles'
import Header from 'src/components/Header'
import { Button } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient'
import { TextInput } from 'react-native-paper'
import colors from 'src/themes/colors'
import { useDispatch, useSelector } from 'react-redux'
import { addTransaction } from 'src/actions/transaction'
import { useNavigation } from '@react-navigation/native'

const showWarningPopup = (message) => Alert.alert(
    'กรุณาตรวจสอบข้อมูล',
    message,
)

export default function AddTransaction(props) {
    const wallet = useMemo(() => props.route.params.wallet, [ props.route.params.wallet ])
    const [ name, setName ] = useState('')
    const [ amount, setAmount ] = useState('')
    const [ type, setType ] = useState('INCOME')
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const profile = useSelector(state => state.profile)

    const amountInput = useRef()

    const validateForm = useCallback(() => {
        if (!name) {
            showWarningPopup('กรุณากรอกชื่อ')
        }
        else if (!amount) {
            showWarningPopup('กรุณากรอก จำนวนเงิน')
        }
        else if (isNaN(amount)) {
            showWarningPopup('จำนวนเงิน ต้องเป็นตัวเลขเท่านั้น')
        }
        else if (parseInt(amount) < 0) {
            showWarningPopup('จำนวนเงิน ต้องห้ามติดลบ')
        }
        else {
            return true
        }
        return false
    }, [ name, amount ])

    const submit = useCallback(() => {
        if (validateForm()) {
            dispatch(addTransaction(
                name,
                type,
                amount,
                wallet.id,
                profile._id,
            ))
            navigation.goBack()
        }
    }, [ amount, dispatch, name, navigation, profile._id, type, validateForm, wallet.id ])

    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.containerContent}>
            <SafeAreaView style={styles.container}>
                <Header title="Add Transaction" />
                <Image
                    source={
                        type === 'INCOME' ?
                            require('src/assets/addtransaction-income.png')
                            :
                            require('src/assets/addtransaction-expense.png')
                    }
                    style={styles.image}
                />
                <View style={styles.formContainer}>
                    <View style={styles.typeContainer}>
                        <TouchableOpacity
                            style={[ styles.typeButtonContainer, styles.typeButtonLeft, type === 'INCOME' ? styles.typeButtonSelected : {} ]}
                            onPress={() => setType('INCOME')}
                        >
                            <Text style={[ styles.typeButtonText, type === 'INCOME' ? styles.typeButtonTextSelected : {} ]}>Income</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[ styles.typeButtonContainer, styles.typeButtonRight, type === 'EXPENSE' ? styles.typeButtonSelected : {} ]}
                            onPress={() => setType('EXPENSE')}
                        >
                            <Text style={[ styles.typeButtonText, type === 'EXPENSE' ? styles.typeButtonTextSelected : {} ]}>Expense</Text>
                        </TouchableOpacity>
                    </View>
                    <TextInput
                        label="Name"
                        mode="outlined"
                        style={styles.input}
                        value={name}
                        onChangeText={setName}
                        returnKeyType="next"
                        onSubmitEditing={() => amountInput.current.focus()}
                    />
                    <TextInput
                        ref={amountInput}
                        label="Amount"
                        mode="outlined"
                        style={styles.input}
                        value={amount}
                        onChangeText={setAmount}
                        returnKeyType="done"
                        keyboardType="decimal-pad"
                        onSubmitEditing={submit}
                    />
                    <Button
                        title="ADD"
                        ViewComponent={LinearGradient}
                        linearGradientProps={{
                            colors: [ colors.secondary, colors.primary ],
                            start: { x: 0, y: 0 },
                            end: { x: 1, y: 1 },
                        }}
                        raised
                        containerStyle={styles.submitButtonContainer}
                        buttonStyle={styles.submitButton}
                        onPress={submit}
                    />
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}
