import { StyleSheet } from 'react-native'
import colors from 'src/themes/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    containerContent: {
        flexGrow: 1,
    },
    image: {
        marginTop: 12,
        width: '100%',
        height: 300,
    },
    formContainer: {
        marginVertical: 32,
        marginHorizontal: 32,
    },
    input: {
        marginBottom: 12,
    },
    submitButtonContainer: {
        marginTop: 24,
    },
    submitButton: {
        borderRadius: 999,
    },
    typeContainer: {
        flexDirection: 'row',
        marginBottom: 32,
    },
    typeButtonContainer: {
        flex: 1,
        borderWidth: 1,
        borderColor: colors.primary,
        paddingVertical: 8,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: '#FFF',
    },
    typeButtonLeft: {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
    },
    typeButtonRight: {
        borderLeftWidth: 0,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
    },
    typeButtonSelected: {
        backgroundColor: '#F27580',
    },
    typeButtonTextSelected: {
        color: '#FFF',
        fontWeight: 'bold',
    },
})
