import React, { useState, useCallback, useRef } from 'react'
import { ScrollView, View, Image, Alert } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import styles from './styles'
import Header from 'src/components/Header'
import { Button } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient'
import { TextInput } from 'react-native-paper'
import colors from 'src/themes/colors'
import { useDispatch, useSelector } from 'react-redux'
import { addWallet } from 'src/actions/wallet'
import { useNavigation } from '@react-navigation/native'

const showWarningPopup = (message) => Alert.alert(
    'กรุณาตรวจสอบข้อมูล',
    message,
)

export default function AddWallet() {
    const [ name, setName ] = useState('')
    const [ initAmount, setInitAmount ] = useState('')
    const profile = useSelector(state => state.profile)
    const dispatch = useDispatch()
    const navigation = useNavigation()

    const initAmountInput = useRef()

    const validateForm = useCallback(() => {
        if (!name) {
            showWarningPopup('กรุณากรอกชื่อ')
        }
        else if (!initAmount) {
            showWarningPopup('กรุณากรอก จำนวนเริ่มต้นของกระเป๋าเงิน')
        }
        else if (isNaN(initAmount)) {
            showWarningPopup('จำนวนเริ่มต้นของกระเป๋าเงิน ต้องเป็นตัวเลขเท่านั้น')
        }
        else if (parseInt(initAmount) < 0) {
            showWarningPopup('จำนวนเริ่มต้นของกระเป๋าเงิน ต้องห้ามติดลบ')
        }
        else {
            return true
        }
        return false
    }, [ name, initAmount ])

    const submit = useCallback(() => {
        if (validateForm()) {
            dispatch(addWallet(name, initAmount, profile._id))
            navigation.navigate('AllWallet')
        }
    }, [ dispatch, initAmount, name, navigation, profile._id, validateForm ])

    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.containerContent}>
            <SafeAreaView style={styles.container}>
                <Header title="Add New Wallet" />
                <Image
                    source={require('src/assets/addwallet.jpg')}
                    style={styles.image}
                />
                <View style={styles.formContainer}>
                    <TextInput
                        label="Name"
                        mode="outlined"
                        style={styles.input}
                        value={name}
                        onChangeText={setName}
                        returnKeyType="next"
                        onSubmitEditing={() => initAmountInput.current.focus()}
                    />
                    <TextInput
                        ref={initAmountInput}
                        label="Initial Amount"
                        mode="outlined"
                        style={styles.input}
                        value={initAmount}
                        onChangeText={setInitAmount}
                        returnKeyType="done"
                        keyboardType="decimal-pad"
                        onSubmitEditing={submit}
                    />
                    <Button
                        title="ADD"
                        ViewComponent={LinearGradient}
                        linearGradientProps={{
                            colors: [ colors.secondary, colors.primary ],
                            start: { x: 0, y: 0 },
                            end: { x: 1, y: 1 },
                        }}
                        raised
                        containerStyle={styles.submitButtonContainer}
                        buttonStyle={styles.submitButton}
                        onPress={submit}
                    />
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}
