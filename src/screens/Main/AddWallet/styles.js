import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    containerContent: {
        flexGrow: 1,
    },
    image: {
        marginTop: 12,
        width: '100%',
        height: 200,
    },
    formContainer: {
        marginVertical: 32,
        marginHorizontal: 32,
    },
    input: {
        marginBottom: 12,
    },
    submitButtonContainer: {
        marginTop: 24,
    },
    submitButton: {
        borderRadius: 999,
    },
})
