import React, { useMemo } from 'react'
import { FlatList, View, Text, ImageBackground, TouchableOpacity } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import styles from './styles'
import { Chip } from 'react-native-paper'
import Icon from 'react-native-vector-icons/Ionicons'
import { useNavigation } from '@react-navigation/native'
import { Avatar } from 'react-native-paper'
import Header from 'src/components/Header'
import { useSelector } from 'react-redux'
import { fundComma, dateTime } from 'src/helpers/text'

export default function Wallet(props) {
    const walletId = useMemo(() => props.route.params.wallet.id, [ props.route.params.wallet.id ])
    const wallet = useSelector(state => state.wallet).find(wallet => wallet.id === walletId)
    const transactions = useSelector(state => state.transaction).filter(transaction => transaction.walletId === wallet.id).reverse()
    const navigation = useNavigation()

    return (
        <FlatList
            data={transactions}
            keyExtractor={(item, index) => index + ''}
            style={styles.container}
            contentContainerStyle={styles.containerContent}
            ListHeaderComponent={
                <>
                    <ImageBackground
                        source={require('src/assets/wallet.png')}
                        style={styles.headerContainer}
                        resizeMode="cover"
                    >
                        <SafeAreaView>
                            <Header />
                            <View style={styles.headerInsetContainer}>
                                <View style={styles.totalContainer}>
                                    <Text style={styles.totalText}>{fundComma(wallet.amount)} ฿</Text>
                                </View>
                            </View>
                        </SafeAreaView>
                    </ImageBackground>
                    {
                        transactions.length > 0 ?
                            (
                                <View style={styles.toolbarContainer}>
                                    <View style={styles.toolbarLeftContainer}>
                                        <Text numberOfLines={1}>
                                            <Text style={styles.walletNameText}>{wallet.name}</Text> Wallet
                                        </Text>
                                    </View>
                                    <Chip
                                        icon="plus"
                                        style={styles.addTransactionButton}
                                        onPress={() => navigation.navigate('AddTransaction', { wallet })}
                                    >
                                        ADD TRANSACTION
                                    </Chip>
                                </View>
                            )
                            : null
                    }
                </>
            }
            renderItem={({ item: transaction }) => (
                <View style={styles.transactionContainer}>
                    <View style={styles.transactionIconContainer}>
                        {
                            transaction.mode === 'INCOME' ?
                                <Icon name="ios-add" size={24} color="#F7A5AC" />
                                :
                                <Icon name="ios-remove" size={24} color="#F7A5AC" />
                        }
                    </View>
                    <View style={styles.transactionTextContainer}>
                        <Text style={styles.transactionNameText}>{transaction.name}</Text>
                        <Text style={styles.transactionDateText}>{dateTime(transaction.createdDate)}</Text>
                    </View>
                    <View style={styles.transactionAmountContainer}>
                        {
                            transaction.mode === 'INCOME' ?
                                <Text style={styles.transactionAmountIncomeText}>+ {fundComma(transaction.amount)} ฿</Text>
                                :
                                <Text style={styles.transactionAmountExpenseText}>- {fundComma(transaction.amount)} ฿</Text>
                        }
                    </View>
                </View>
            )}
            ListEmptyComponent={
                <View style={styles.emptyListContainer}>
                    <Text style={styles.emptyListMainText}>WALLET</Text>
                    <Text style={styles.emptyListSubText}>Piggy</Text>
                    <TouchableOpacity
                        style={styles.emptyListAddTransactionButton}
                        onPress={() => navigation.navigate('AddTransaction', { wallet })}
                    >
                        <Avatar.Icon size={192} icon="plus" />
                        <Text style={styles.emptyListAddTransactionButtonText}>To get start, Add New Transaction</Text>
                    </TouchableOpacity>
                </View>
            }
        />
    )
}
