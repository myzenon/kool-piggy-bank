import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    contentContainerStyle: {
        flexGrow: 1,
        paddingBottom: 16,
    },
    changePasswordImage: {
        marginTop: 12,
        width: '100%',
        height: 250,
    },
    contentContaier: {
        marginBottom: 32,
    },
    formContainer: {
        marginTop: 18,
        marginHorizontal: 32,
    },
    input: {
        paddingHorizontal: 0,
        marginBottom: 18,
    },
    actionContainer: {
        marginTop: 24,
    },
    changeProfileButton: {
        borderRadius: 999,
    },
})
