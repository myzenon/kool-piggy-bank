import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    contentContainerStyle: {
        flexGrow: 1,
        paddingBottom: 16,
    },
    mainText: {
        fontWeight: 'bold',
        fontSize: 24,
        marginHorizontal: 32,
    },
    subText: {
        fontSize: 20,
        marginHorizontal: 32,
    },
    registerImage: {
        marginTop: 18,
        width: 240,
        height: 250,
    },
    contentContaier: {
        marginTop: 40,
        marginBottom: 32,
    },
    formContainer: {
        marginTop: 18,
        marginHorizontal: 32,
    },
    input: {
        paddingHorizontal: 0,
        marginBottom: 18,
    },
    actionContainer: {
        marginTop: 24,
    },
    registerButton: {
        borderRadius: 999,
    },
    backButton: {
        paddingVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backText: {
        color: '#BBB',
        fontSize: 12,
    },
})
