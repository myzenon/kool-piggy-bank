import React from 'react'
import { View, Text } from 'react-native'
import { IconButton } from 'react-native-paper'
import styles from './styles'
import { useNavigation } from '@react-navigation/native'

export default function Header (props) {
    const navigation = useNavigation()
    return (
        <View style={styles.container}>
            <IconButton
                icon="arrow-left"
                size={24}
                color="#888"
                onPress={() => navigation.goBack()}
            />
            <Text style={styles.titleText}>{props.title}</Text>
        </View>
    )
}
