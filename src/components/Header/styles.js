import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 12,
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 18,
        marginLeft: 4,
        color: '#444',
    },
})
