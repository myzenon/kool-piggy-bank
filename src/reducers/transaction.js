import { ADD_TRANSACTION } from 'src/actions/transaction'
import uuid from 'react-native-uuid'

export default (state = [], action) => {
    switch (action.type) {
        case ADD_TRANSACTION:
            return [
                ...state,
                {
                    id: uuid.v4(),
                    name: action.name,
                    mode: action.mode,
                    amount: action.amount,
                    walletId: action.walletId,
                    userId: action.userId,
                    createdDate: new Date(),
                },
            ]
        default:
            return state
    }
}
