import { combineReducers } from 'redux'
import authToken from './authToken'
import profile from './profile'
import wallet from './wallet'
import transaction from './transaction'

export default combineReducers({
    authToken,
    profile,
    wallet,
    transaction,
})
